<?php

namespace Drupal\msg_plugin_custom\Plugins\msg_plugin\messenger;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   plugin_id = "custom",
 *   title = @Translation("Custom messenger"),
 *   description = @Translation("Displays: Custom text entered by the user."),
 *   derivative = "Drupal\msg_plugin_custom\Plugins\Derivatives\Custom"
 * )
 */
class Custom extends PluginBase {
  public function __construct(array $configuration, $plugin_id, DiscoveryInterface $discovery) {
    parent::__construct($configuration, $plugin_id, $discovery);
    $this->definition = $this->discovery->getDefinition($this->plugin_id);
  }
  public function message() {
    return $this->definition['description'];
  }
}