<?php

namespace Drupal\msg_plugin_custom\Plugins\Derivatives;

use Drupal\Component\Plugin\Derivative\DerivativeInterface;

class Custom implements DerivativeInterface {

  /**
   * Implements DerivativeInterface::getDerivativeDefinition().
   */
  public function getDerivativeDefinition($derivative_id, array $base_plugin_definition) {
    $derivatives = $this->getDerivativeDefinitions($base_plugin_definition);
    if (isset($derivatives[$derivative_id])) {
      return $derivatives[$derivative_id];
    }
  }

  /**
   * Implements DerivativeInterface::getDerivativeDefinitions().
   */
  public function getDerivativeDefinitions(array $base_plugin_definition) {
    $query = db_select('msg_plugin_custom', 'mpc')
      ->fields('mpc', array('mid', 'title', 'message'));
    $results = $query->execute();
    $derivatives = array();
    foreach ($results as $result) {
      $derivatives[$result->mid] = array(
        'title' => $result->title,
        'description' => $result->message,
      ) + $base_plugin_definition;
    }
    return $derivatives;
  }
}
