<?php

namespace Drupal\msg_plugin\Plugins\msg_plugin\messenger;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   plugin_id = "violets",
 *   title = @Translation("Violets"),
 *   description = @Translation("Violets are blue.")
 * )
 */
class Violets extends PluginBase {
  public function message() {
    return t('Violets are blue.');
  }
}
