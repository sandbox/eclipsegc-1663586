<?php

namespace Drupal\msg_plugin\Plugins\msg_plugin\messenger;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   plugin_id = "roses",
 *   title = @Translation("Roses"),
 *   description = @Translation("Roses are red.")
 * )
 */
class Roses extends PluginBase {
  public function message() {
    return t('Roses are red.');
  }
}