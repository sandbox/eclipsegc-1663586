<?php

namespace Drupal\msg_plugin\Plugins\Type;

use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Core\Plugin\Discovery\DirectoryDiscovery;
use Drupal\Component\Plugin\Discovery\DerivativeDiscoveryDecorator;
use Drupal\Component\Plugin\Factory\DefaultFactory;

class Messenger extends PluginManagerBase {
  public function __construct() {
    $this->discovery = new DerivativeDiscoveryDecorator(new DirectoryDiscovery('msg_plugin', 'messenger'));
    $this->factory = new DefaultFactory($this);
  }
}
