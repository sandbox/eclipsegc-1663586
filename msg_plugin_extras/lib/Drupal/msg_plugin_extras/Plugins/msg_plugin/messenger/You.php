<?php

namespace Drupal\msg_plugin_extras\Plugins\msg_plugin\messenger;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   plugin_id = "you",
 *   title = @Translation("You"),
 *   description = @Translation("I hope you think so too.")
 * )
 */
class You extends PluginBase {
  public function message() {
    return t('I hope you think so too.');
  }
}