<?php

namespace Drupal\msg_plugin_extras\Plugins\msg_plugin\messenger;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   plugin_id = "plugins",
 *   title = @Translation("Plugins"),
 *   description = @Translation("Plugins are awesome.")
 * )
 */
class Plugins extends PluginBase {
  public function message() {
    return t('Plugins are awesome.');
  }
}